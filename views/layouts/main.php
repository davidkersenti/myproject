<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
   <?php

            NavBar::begin([
                'brandLabel' => 'Project Managment',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => '',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
						
															            Yii::$app->user->isGuest ?
			['label' => 'Erd', 'url' => ['/site/contact']]
			:
							['label' => 'Events',
							'items' => [
                            ['label' => 'School Events', 'url' => ['/event/index']],

                        ],
                    ],
					
			
											Yii::$app->user->isGuest ?
			['label' => '', 'url' => ['/site/']]
			:
							['label' => 'Courses',
							'items' => [
                            ['label' => 'Courses', 'url' => ['/course/index']],
                            ['label' => 'Course Classes', 'url' => ['/course-class/index']],
						 ['label' => 'Student Course Class', 'url' => ['/student-agrigation/index']],

                        ],
                    ],
			
								            Yii::$app->user->isGuest ?
			['label' => 'Home', 'url' => ['/site']]
			:
							['label' => 'Classes',
							'items' => [
                            ['label' => 'Classes', 'url' => ['/classname/index']],
                            ['label' => 'Course Classes', 'url' => ['/course-class/index']],
						 ['label' => 'Student Course Class', 'url' => ['/student-agrigation/index']],

                        ],
                    ],
			
								            Yii::$app->user->isGuest ?
			['label' => 'About', 'url' => ['/site/about']]
			:
							['label' => 'Students',
							'items' => [
                            ['label' => 'Students', 'url' => ['/student/index']],
                            ['label' => 'Student Teacher', 'url' => ['/student-teacher/index']],
						 ['label' => 'Student Course Class', 'url' => ['/student-agrigation/index']],

                        ],
                    ],
			
			
			
										            Yii::$app->user->isGuest ?
			['label' => '', 'url' => ['/site/']]
			:
							['label' => 'Users',
							'items' => [
                            ['label' => 'All Users', 'url' => ['/user/index']],
                            ['label' => 'Teachers', 'url' => ['/teacher/index']],
							['label' => 'Principals', 'url' => ['/principal/index']],
							['label' => 'secretary', 'url' => ['/secretary/index']],
                        ],
                    ],
					

		
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
