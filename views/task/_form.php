<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Status;

use app\models\Level;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'taskName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'startDate')->textInput() ?>

    <?= $form->field($model, 'planeDate')->textInput() ?>

    <?= $form->field($model, 'endDate')->textInput() ?>

       <?= $form->field($model, 'user')->
				dropDownList(User::getUser())  ?>

              <?= $form->field($model, 'level')->

				dropDownList(Level::getLevels())  ?>

       <?= $form->field($model, 'status')->

				dropDownList(Status::getStatus())  ?>

    <?= $form->field($model, 'created_at')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
