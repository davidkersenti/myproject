<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\TeamLeader;
use app\models\Urgency;
use app\models\Status;
use app\models\Task;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'projectName')->textInput(['maxlength' => true]) ?>

      <?= $form->field($model, 'teamleader')->
				dropDownList(Teamleader::getTeamLeader())  ?>

    <?= $form->field($model, 'user')->
				dropDownList(User::getUser())  ?>

         <?= $form->field($model, 'task')->
				dropDownList(Task::getTask())  ?>

    <?= $form->field($model, 'startDate')->textInput() ?>

    <?= $form->field($model, 'planDate')->textInput() ?>

    <?= $form->field($model, 'endDate')->textInput() ?>

    <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

          <?= $form->field($model, 'urgency')->
				dropDownList(Urgency::getUrgency())  ?>

       <?= $form->field($model, 'status')->
				dropDownList(Status::getStatus())  ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
