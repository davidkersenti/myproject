<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "teamleader".
 *
 * @property integer $teamleaderId
 * @property string $teamleaderName
 *
 * @property Project[] $projects
 */
class Teamleader extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teamleader';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teamleaderName'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'teamleaderId' => 'Teamleader ID',
            'teamleaderName' => 'Teamleader Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['teamleaderId' => 'teamleaderId']);
    }
        	public static function getTeamLeader()
	{
		$allLeaders = self::find()->all();
		$allLeadersArray = ArrayHelper::
					map($allLeaders, '	teamleaderId', 'teamleaderName');
		return $allLeadersArray;						
	}
}
