<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

use yii\db\ActiveRecord;
use app\models\Role;
/**
 * This is the model class for table "user".
 *
 * @property string $firstName
 * @property string $lastName
 * @property string $email
 * @property string $address
 * @property integer $id
 * @property integer $role
 * @property string $username
 * @property string $password
 * @property string $authKey
 * @property string $created_at
 *
 * @property Project[] $projects
 * @property Task[] $tasks
 * @property Role $role0
 */
class User extends \yii\db\ActiveRecord  implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role'], 'integer'],
            [['firstName', 'lastName','email','	id','username','password','authKey','created_at'], 'required'],
            [['firstName', 'lastName', 'email', 'address', 'username', 'password', 'authKey', 'created_at'], 'string', 'max' => 255],
            [['role'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['role' => 'roleId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'firstName' => 'First Name',
            'lastName' => 'Last Name',
            'email' => 'Email',
            'address' => 'Address',
            'id' => 'ID',
            'role' => 'Role',
            'username' => 'Username',
            'password' => 'Password',
            'authKey' => 'Auth Key',
            'created_at' => 'Created At',
        ];
    }
        public function getAuthKey()
    {
        return $this->authKey;
    }
     public function getId()
    {
        return $this->id;
    }

     public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public static function findIdentity($id){

       return self::findOne($id);
    }

     public static function findIdentityByAccessToken($token, $type = null)
     {
             throw new NotSupportedException('Not Supported');
     }
        
          public static function findByUsername($username)
    {
		return  self::findOne(['username'=> $username]);
	}

        public function validatePassword($password)
    {
        return $this->password == $password;
    }
    private function isCorrectHash($plaintext, $hash)
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}
    public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
				generatePasswordHash($this->password);

        if ($this->isNewRecord)
		    $this->authKey = Yii::$app->security->generateRandomString(32);

        return $return;
    }	
	

    public function afterSave($insert,$changedAttributes)
    {
        $return = parent::afterSave($insert, $changedAttributes);
	
		$auth = Yii::$app->authManager;
		$roleName = $this->role; 
		$role = $auth->getRole($roleName);
		if (\Yii::$app->authManager->getRolesByUser($this->id) == null){
			$auth->assign($role, $this->id);
		
        }
        return $return;
    }	
	
 	public function getFullname()
    {
        return $this->firstname.' '.$this->lastname;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
       public function getRole0()
    {
        return $this->hasOne(Role::className(), ['roleId' => 'role']);
    }


        	public static function getUser()
	{
		$allUser = self::find()->all();
		$allUserArray = ArrayHelper::
					map($allUser, 'id', 'username');
		return $allUserArray;						
	}
   
       
}