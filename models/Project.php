<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project".
 *
 * @property integer $projectId
 * @property string $projectName
 * @property integer $teamleaderId
 * @property integer $userId
 * @property integer $taskId
 * @property string $startDate
 * @property string $planDate
 * @property string $endDate
 * @property string $location
 * @property integer $urgencyId
 * @property integer $statusId
 * @property string $description
 *
 * @property Status $status
 * @property Task $task
 * @property Teamleader $teamleader
 * @property Urgency $urgency
 * @property User $user
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teamleaderId', 'userId', 'taskId', 'urgencyId', 'statusId'], 'integer'],
            [['startDate', 'planDate', 'endDate'], 'safe'],
            [['projectName', 'startDate','planDate','endDate','location','description'], 'required'],
            [['description'], 'string'],
            [['projectName', 'location'], 'string', 'max' => 255],
            [['statusId'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['statusId' => 'statusId']],
            [['taskId'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['taskId' => 'taskId']],
            [['teamleaderId'], 'exist', 'skipOnError' => true, 'targetClass' => Teamleader::className(), 'targetAttribute' => ['teamleaderId' => 'teamleaderId']],
            [['urgencyId'], 'exist', 'skipOnError' => true, 'targetClass' => Urgency::className(), 'targetAttribute' => ['urgencyId' => 'urgencyId']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'projectId' => 'Project ID',
            'projectName' => 'Project Name',
            'teamleaderId' => 'Teamleader ID',
            'userId' => 'User ID',
            'taskId' => 'Task ID',
            'startDate' => 'Start Date',
            'planDate' => 'Plan Date',
            'endDate' => 'End Date',
            'location' => 'Location',
            'urgencyId' => 'Urgency ID',
            'statusId' => 'Status ID',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['statusId' => 'statusId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['taskId' => 'taskId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamleader()
    {
        return $this->hasOne(Teamleader::className(), ['teamleaderId' => 'teamleaderId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUrgency()
    {
        return $this->hasOne(Urgency::className(), ['urgencyId' => 'urgencyId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}
