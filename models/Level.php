<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "level".
 *
 * @property integer $levelId
 * @property string $levelname
 *
 * @property Task[] $tasks
 */
class level extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['levelname'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'levelId' => 'Level ID',
            'levelname' => 'Levelname',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['levelId' => 'levelId']);
    }
         	public static function getLevels()
	{
		$allLevels = self::find()->all();
		$allLevelsArray = ArrayHelper::
					map($allLevels, 'levelId', 'levelname');
		return $allLevelsArray;						
	}
}
