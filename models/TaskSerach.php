<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Task;

/**
 * TaskSerach represents the model behind the search form about `app\models\Task`.
 */
class TaskSerach extends Task
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['taskId', 'userId', 'levelId', 'statusId'], 'integer'],
            [['taskName', 'startDate', 'planeDate', 'endDate', 'created_at', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Task::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'taskId' => $this->taskId,
            'startDate' => $this->startDate,
            'planeDate' => $this->planeDate,
            'endDate' => $this->endDate,
            'userId' => $this->userId,
            'levelId' => $this->levelId,
            'statusId' => $this->statusId,
        ]);

        $query->andFilterWhere(['like', 'taskName', $this->taskName])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
