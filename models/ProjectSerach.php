<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Project;

/**
 * ProjectSerach represents the model behind the search form about `app\models\Project`.
 */
class ProjectSerach extends Project
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['projectId', 'teamleaderId', 'userId', 'taskId', 'urgencyId', 'statusId'], 'integer'],
            [['projectName', 'startDate', 'planDate', 'endDate', 'location', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Project::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'projectId' => $this->projectId,
            'teamleaderId' => $this->teamleaderId,
            'userId' => $this->userId,
            'taskId' => $this->taskId,
            'startDate' => $this->startDate,
            'planDate' => $this->planDate,
            'endDate' => $this->endDate,
            'urgencyId' => $this->urgencyId,
            'statusId' => $this->statusId,
        ]);

        $query->andFilterWhere(['like', 'projectName', $this->projectName])
            ->andFilterWhere(['like', 'location', $this->location])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
