<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "task".
 *
 * @property integer $taskId
 * @property string $taskName
 * @property string $startDate
 * @property string $planeDate
 * @property string $endDate
 * @property integer $userId
 * @property integer $levelId
 * @property integer $statusId
 * @property string $created_at
 * @property string $description
 *
 * @property Project[] $projects
 * @property Level $level
 * @property Status $status
 * @property User $user
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['startDate', 'planeDate', 'endDate'], 'safe'],
            [['userId', 'levelId', 'statusId'], 'integer'],
            [['taskName', 'startDate','endDate','planeDate','description','created_at'], 'required'],
            [['description'], 'string'],
            [['taskName', 'created_at'], 'string', 'max' => 255],
            [['levelId'], 'exist', 'skipOnError' => true, 'targetClass' => Level::className(), 'targetAttribute' => ['levelId' => 'levelId']],
            [['statusId'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['statusId' => 'statusId']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'taskId' => 'Task ID',
            'taskName' => 'Task Name',
            'startDate' => 'Start Date',
            'planeDate' => 'Plane Date',
            'endDate' => 'End Date',
            'userId' => 'User ID',
            'levelId' => 'Level ID',
            'statusId' => 'Status ID',
            'created_at' => 'Created At',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['taskId' => 'taskId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevel()
    {
        return $this->hasOne(Level::className(), ['levelId' => 'levelId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['statusId' => 'statusId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
        	public static function gettask()
	{
		$alltask = self::find()->all();
		$alltaskArray = ArrayHelper::
					map($alltask, 'taskId', 'taskName');
		return $alltaskArray;						
	}
}
