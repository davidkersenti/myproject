<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "status".
 *
 * @property integer $statusId
 * @property string $statusname
 *
 * @property Project[] $projects
 * @property Task[] $tasks
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['statusname'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'statusId' => 'Status ID',
            'statusname' => 'Statusname',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['statusId' => 'statusId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['statusId' => 'statusId']);
    }
         	public static function getStatus()
	{
		$allStatus = self::find()->all();
		$allStatusArray = ArrayHelper::
					map($allStatus, 'statusId', 'statusname');
		return $allStatusArray;						
	}
}
