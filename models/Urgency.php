<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "urgency".
 *
 * @property integer $urgencyId
 * @property string $urgencyName
 *
 * @property Project[] $projects
 */
class Urgency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'urgency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['urgencyName'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'urgencyId' => 'Urgency ID',
            'urgencyName' => 'Urgency Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['urgencyId' => 'urgencyId']);
    }
       	public static function getUrgency()


	{
		$allUrgency = self::find()->all();
		$allUrgencyArray = ArrayHelper::
					map($allUrgency, 'urgencyId', 'urgencyName');
		return $allUrgencyArray;						
	}
}
