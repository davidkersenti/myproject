<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\UrlManager\Url;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
          
        
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    
    //////////////////////////////////// RBAC ////////////////////////////////////////
    
public function actionRole()
	{
		$auth = Yii::$app->authManager;	

        $ceo = $auth->createRole('ceo');
		$auth->add($ceo);			
		
		$task_perform = $auth->createRole('task_perform');
		$auth->add($task_perform);
		
		$project_manager = $auth->createRole('project_manager');
		$auth->add($project_manager);

    	$admin = $auth->createRole('admin');
		$auth->add($admin);
	}


    public function actionCeopermissions()
	{
		$auth = Yii::$app->authManager;

		$viewProject = $auth->createPermission('viewProject'); 
		$viewProject->description = 'View Project ';
		$auth->add($viewProject);

        $indexProject = $auth->createPermission('indexProject'); 
		$indexProject->description = 'Index Project ';
		$auth->add($indexProject);   
	}

	public function actionTaskpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$changeStatus = $auth->createPermission('changeStatus');
		$changeStatus->description = 'Change the status task';
		$auth->add($changeStatus);

        $viewTask = $auth->createPermission('viewTask'); 
		$viewTask->description = 'View Task ';
		$auth->add($viewTask);	

        $indexTask = $auth->createPermission('indexTask'); 
		$indexTask->description = 'Index Task ';
		$auth->add($indexTask);	

         $editTask = $auth->createPermission('editTask'); 
		$editTask->description = 'Edit Task ';
		$auth->add($editTask);		
		
	}


	public function actionManagerpermissions()
	{
		$auth = Yii::$app->authManager;

		$assignUserToProject = $auth->createPermission('assignUserToProject'); 
		$assignUserToProject->description = 'Assign User To Project';
		$auth->add($assignUserToProject);	

		$assignUserToTask = $auth->createPermission('assignUserToTask'); 
		$assignUserToTask->description = 'Assign User To Task';
		$auth->add($assignUserToTask);	

        $createTask = $auth->createPermission('createTask'); 
		$createTask->description = 'Create Task ';
		$auth->add($createTask);

        $deleteTask = $auth->createPermission('deleteTask'); 
		$deleteTask->description = 'Delete Task ';
		$auth->add($deleteTask);
	}

	
	public function actionAdminpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$createProject = $auth->createPermission('createProject'); 
		$createProject->description = 'Admin can create projects';
		$auth->add($createProject);

		$deleteProject = $auth->createPermission('deleteProject');  
		$deleteProject->description = 'Admin can delete projects';
		$auth->add($deleteProject);

        $editProject = $auth->createPermission('editProject');  
		$editProject->description = 'Admin can edit Project';
		$auth->add($editProject);

        $createUser = $auth->createPermission('createUser'); 
		$createUser->description = 'Admin can create users';
		$auth->add($createUser);

        $viewUser = $auth->createPermission('viewUser'); 
		$createUser->description = 'Admin can view users';
		$auth->add($viewUser);

        $indexUser = $auth->createPermission('indexUser'); 
		$indexUser->description = 'Admin can index users';
		$auth->add($indexUser);

         $editUser = $auth->createPermission('editUser'); 
		$indexUser->description = 'Admin can edit users';
		$auth->add($editUser);

        $deleteUser = $auth->createPermission('deleteUser'); 
		$deleteUser->description = 'Admin can delete users';
		$auth->add($deleteUser);
        
	
	}

	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				

        $ceo = $auth->getRole('ceo');

        $viewProject = $auth->getPermission('viewProject'); 
		$auth->addChild($ceo, $viewProject); 
         

        $indexProject = $auth->getPermission('indexProject'); 
        $auth->addChild($ceo, $indexProject); 

        

        ////////////////
        $task_perform = $auth->getRole('task_perform'); 

		$changeStatus = $auth->getPermission('changeStatus'); 
		$auth->addChild($task_perform, $changeStatus);
		
		$editTask = $auth->getPermission('editTask'); 
		$auth->addChild($task_perform, $editTask);

        $viewTask = $auth->getPermission('viewTask'); 
		$auth->addChild($task_perform, $viewTask);

        $indexTask = $auth->getPermission('indexTask'); 
		$auth->addChild($task_perform, $indexTask);


        /////////////////////////////


		$project_manager = $auth->getRole('project_manager'); 
		$auth->addChild($project_manager, $ceo);
        $auth->addChild($project_manager, $task_perform);
		
		$assignUserToProject = $auth->getPermission('assignUserToProject'); 
		$auth->addChild($project_manager, $assignUserToProject);

		$assignUserToTask = $auth->getPermission('assignUserToTask'); 
		$auth->addChild($project_manager, $assignUserToTask);	

        $createTask = $auth->getPermission('createTask'); 
		$auth->addChild($project_manager, $createTask);

        $editTask = $auth->getPermission('editTask'); 
		$auth->addChild($project_manager, $editTask);

        $deleteTask = $auth->getPermission('deleteTask'); 
		$auth->addChild($project_manager, $deleteTask);

        
		///////////////////////////////////////
		
		$admin = $auth->getRole('admin'); 
		$auth->addChild($admin, $project_manager);	
		
		$createProject = $auth->getPermission('createProject'); 
		$auth->addChild($admin, $createProject);
			
	   $deleteProject = $auth->getPermission('deleteProject'); 
		$auth->addChild($admin, $deleteProject);
		
		$editProject = $auth->getPermission('editProject'); 
		$auth->addChild($admin, $editProject);

        $createUser = $auth->getPermission('createUser'); 
		$auth->addChild($admin, $createUser);

        $deleteUser = $auth->getPermission('deleteUser'); 
		$auth->addChild($admin, $deleteUser);

        $editUser = $auth->getPermission('editUser'); 
		$auth->addChild($admin, $editUser);

        $viewUser = $auth->getPermission('viewUser'); 
		$auth->addChild($admin, $viewUser);

        $indexUser = $auth->getPermission('indexUser'); 
		$auth->addChild($admin, $indexUser);

	}


}
