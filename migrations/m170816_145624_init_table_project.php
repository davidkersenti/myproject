<?php

use yii\db\Migration;

class m170816_145624_init_table_project extends Migration
{
     public function up()
    {


     $this->createTable('project', [
             'projectId'  => 'pk',
			'projectName' => 'string',
           'teamleaderId' =>  'integer', //מפתח זר
            'userId'  => 'integer', // פתח זר
            'taskId'  => 'integer', // פתח זר
			'startDate' => $this->date(),
            'planDate' => $this->date(),
			'endDate' => $this->date(),
            'location' => 'string',
            'urgencyId'  => 'integer', // פתח זר
            'statusId' => 'integer', // פתח זר
             'description'  => $this->text(),
            
        
        
           
            
		]);

         $this->addForeignKey(
            'fk-project-teamleaderId',// This is the fk => the table where i want the fk will be
            'project',// son table
            'teamleaderId', // son pk	
            'teamleader', // father table
            'teamleaderId', // father pk
            'CASCADE'
			);
         $this->addForeignKey(
            'fk-project-userId',// This is the fk => the table where i want the fk will be
            'project',// son table
            'userId', // son pk	
            'user', // father table
           'id', // father pk
            'CASCADE'
			);

            $this->addForeignKey(
            'fk-project-taskId',// This is the fk => the table where i want the fk will be
            'project',// son table
            'taskId', // son pk	
            'task', // father table
            'taskId', // father pk
            'CASCADE'
			);

            $this->addForeignKey(
            'fk-project-urgencyId',// This is the fk => the table where i want the fk will be
            'project',// son table
            'urgencyId', // son pk	
            'urgency', // father table
            'urgencyId', // father pk
            'CASCADE'
			);

            $this->addForeignKey(
            'fk-project-statusId',// This is the fk => the table where i want the fk will be
            'project',// son table
            'statusId', // son pk	
            'status', // father table
            'statusId', // father pk
            'CASCADE'
			);
            



    }
    public function down()
    {
        echo "m170816_145624_init_table_project cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
