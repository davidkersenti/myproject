<?php

use yii\db\Migration;

class m170816_143018_init_table_role extends Migration
{
         public function up()

    {

        $this->createTable('role', [
            'roleId' => 'pk' ,
            'rolename'  => 'string',
            
		]);
   }

    public function down()
    {
        echo "m170816_143018_init_table_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
