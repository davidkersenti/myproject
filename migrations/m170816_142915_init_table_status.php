<?php

use yii\db\Migration;

class m170816_142915_init_table_status extends Migration
{
           public function up()
    {


        $this->createTable('status', [
            'statusId' => 'pk' ,
            'statusname'  => 'string',
            
		]);
   }
    public function down()
    {
        echo "m170816_142915_init_table_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
