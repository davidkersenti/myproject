<?php

use yii\db\Migration;

class m170816_143825_init_table_task extends Migration
{
      public function up()

    {


             $this->createTable('task', [
             'taskId'  => 'pk',
			'taskName' => 'string',
			'startDate' => $this->date(),
            'planeDate' => $this->date(),
			'endDate' => $this->date(),
            'userId' => 'integer', /// מפתח זר
            'levelId'  => 'integer', // פתח זר
            'statusId' => 'integer', ///מפתח זר
			'created_at'  => 'string',
            'description'  => $this->text(),
		]);

         $this->addForeignKey(
            'fk-task-userId',// This is the fk => the table where i want the fk will be
            'task',// son table
            'userId', // son pk	
            'user', // father table
            'id', // father pk
            'CASCADE'
			);

        
        	 $this->addForeignKey(
            'fk-task-levelId',// This is the fk => the table where i want the fk will be
            'task',// son table
            'levelId', // son pk	
            'level', // father table
            'levelId', // father pk
            'CASCADE'
			);

            
               $this->addForeignKey(
            'fk-task-statusId',// This is the fk => the table where i want the fk will be
            'task',// son table
            'statusId', // son pk	
            'status', // father table
            'statusId', // father pk
            'CASCADE'
			);



    }

    public function down()
    {
        echo "m170816_143825_init_table_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
