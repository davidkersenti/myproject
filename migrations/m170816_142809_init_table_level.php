<?php

use yii\db\Migration;

class m170816_142809_init_table_level extends Migration
{
           public function up()
    {


        $this->createTable('level', [
            'levelId' => 'pk' ,

            'levelname'  => 'string',
           
		]);
   }

    public function down()
    {
        echo "m170816_142809_init_table_level cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
