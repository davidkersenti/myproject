<?php

use yii\db\Migration;

class m170816_144206_init_table_teamleader extends Migration
{
      public function up()
    {

        $this->createTable('teamleader', [
            'teamleaderId' => 'pk' ,
            'teamleaderName'  => 'string',
            

		]);
       }

    public function down()
    {
        echo "m170816_144206_init_table_teamleader cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
